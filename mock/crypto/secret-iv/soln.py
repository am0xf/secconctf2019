#!/usr/bin/env python3

from Crypto.Cipher import AES
import codecs

key = b'YELLOW SUBMARINE'
block_size = 16

def decrypt(ciphertext, IV):
	obj = AES.new(key, AES.MODE_CBC,IV)
	plaintext = obj.decrypt(ciphertext)

	return plaintext

plaintext = 'Welcome to InCTFj finals! Hope you are having a great time here ;)'
ciphertext = codecs.decode('1614ad13f934ca0e410da7785ddb3b248377eab15cb6cd46f355f5896ce848ce6675b8d48f284bf4186c982ac74f9f9302d2d0c78ab3cd87fd61f5fa02003b38418c2e33c09882de5da99a8066a2f4e7', 'hex') 

iv = (int.from_bytes(b'>\x0b\x0f\x17\t\x07\x1eG\x06W\x7f#\x01!u;', 'big') ^ int.from_bytes(plaintext[:16].encode('ascii'), 'big')).to_bytes(16, 'big')
print(iv)
print(decrypt(ciphertext, iv))

