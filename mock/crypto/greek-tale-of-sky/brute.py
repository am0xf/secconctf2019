#!/usr/bin/env python3

#This is the encryption pattern used for Tale of the Sky
import random
import codecs

def brute(message):
	for turns in range(3, 13):
		ct = ''
		for i in range(turns):
			for j in range(0, len(message)-i, turns):
				ct += message[i+j]

		if ct.startswith('inctfj{') and ct.endswith('}'):
			print(ct)

d = open('ciphertext.txt', 'r').read()[:-1]
m = codecs.decode(codecs.decode(d, 'hex'), 'ascii')
brute(m)

