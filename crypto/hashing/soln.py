#!/usr/bin/env python3

# Risky (my soln): say s = xm + yn where m > n and we know m and n. so to find x we brute force until the value vm > s. then x = v - 1. same for y
# Sure : last value added is k * 1 where k < 256. so mod hash with 256 and get it. the subtract k from hash and continue

L = 24
s = ''
hsh = 2555096369405375265614952760773872147794992125129312793139
cur_hsh = hsh

for i in range(L):
	for j in range(1, 256):
		if 256 ** (23 - i) * j > cur_hsh:
			break

	j -= 1
	cur_hsh -= 256 ** (23 - i) * j

	s += chr(j)

print(s)

