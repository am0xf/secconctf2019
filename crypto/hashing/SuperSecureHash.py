from secret import flag

def SuperSecureHash(message, base, order):
	assert(len(message)==24)
        result = 0
        L = len(message)
        for i in range(L):
                result += ( ord(message[i]) * base**(L - i - 1) ) % order 
        print "SuperSecureHash=", result
 
SuperSecureHash(flag, 256, 10**60)

#SuperSecureHash= 2555096369405375265614952760773872147794992125129312793139

