#!/usr/bin/env python
import os
from Crypto.Cipher import AES
from Crypto.Util.number import *
from secret import FLAG

key1 = os.urandom(16)

class Unbuffered(object):
   def __init__(self, stream):
       self.stream = stream
   def write(self, data):
       self.stream.write(data)
       self.stream.flush()
   def writelines(self, datas):
       self.stream.writelines(datas)
       self.stream.flush()
   def __getattr__(self, attr):
       return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

def pad(m):
    tmp=16-len(m)%16
    return m+chr(tmp)*tmp

def unpad(c):
    polarity = ord(c[-1])
    if polarity > 15: raise AssertionError("Incorrect Padding")
    return c[:-polarity]

def admin_decrypt(c) :
    cipher = AES.new(key1, AES.MODE_CBC, FLAG)
    return (cipher.decrypt(c))

def Encrypt(inp,iv):
    cipher = AES.new(key1, AES.MODE_CBC, iv)
    return (iv + cipher.encrypt(pad(inp))).encode('hex')

def Decrypt(c):
    c = c.decode('hex')
    iv,c = c[:16], c[16:]
    cipher = AES.new(key1, AES.MODE_CBC, iv)
    #from IPython import embed; embed()
    return unpad(cipher.decrypt(c))

def banner():
    TITLE = "Encryption/Decryption Service"
    print('=' * len(TITLE))
    print(TITLE)
    print('=' * len(TITLE))

    MENU = """
    1) Encrypt
    2) Decrypt
    3) Exit
    """
    print(MENU)


if __name__ == "__main__" :
    print(chr(27) + "[2J")
    count = 0
    while count < 3 :
        banner()
        ch = int(raw_input("Enter your choice :"))
        iv = os.urandom(16)
        if ch == 1 :
            m = raw_input("Enter the plaintext :")
            if 'admin' in m :
                print "Username can't be admin"
                continue # or break instead
            print Encrypt(m,iv)
            continue
        elif ch == 2 :
            c = raw_input("Enter the ciphertext(in hex) :")
            m = Decrypt(c)
            if m[:5] == "admin" :
                print "Welcome admin !"
                print "Admin decryption service :"
                ct= raw_input("Enter the ciphertext(in hex) :").decode('hex')
                if len(ct) > 32:
                    print("Ciphertext can't be greater than 32 bytes")
                    continue
                print admin_decrypt(ct)
            else :
                print m

        else:
            break

        count += 1
    print("See you next time!")
