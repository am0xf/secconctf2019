#!/usr/bin/env python3

import codecs

s = codecs.decode(input(), 'hex')

iv = s[:16]
ct = s[16:]
iv = iv[0].to_bytes(1, 'big') + iv[2:5] + b'\x00' + iv[5:]
ct = ct[0].to_bytes(1, 'big') + ct[2:5] + b'\x00' + ct[5:]
print(codecs.encode(iv+ct, 'hex'))

