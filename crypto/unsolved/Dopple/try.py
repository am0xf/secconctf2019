#!/usr/bin/env python3

from gmpy2 import *

def egcd(a, b):
	if a == 0:
		return (b, 0, 1)
	else:
		g, y, x = egcd(b % a, a)
		return (g, x - (b // a) * y, y)

def modinv(a, m):
	g, x, y = egcd(a, m)
	if g != 1:
		raise Exception('modular inverse does not exist')
	else:
		return x % m

data = open('ciphertext').read()

ct = int(data.split(' ')[0].split('(')[2].split(')')[0])
ct = mpz(ct)
n = int(data.split(' ')[1].split('(')[1].split(')')[0])
n = mpz(n)

p = mpz(2 ** 64)
f = 0

for y in range(2 ** 30):

	b = 2 ** 64 - y
	p = (-b + sqrt(b * b + 4 * n)) // 2; 

	if n % p == 0:
		print(n%p)
		f = 1
		break

if f == 1:
	q = n // p
	tot = (p-1) * (q-1)
	e = 65537
	d = modinv(e, tot)

	pn = pow(ct, d, n)

	dpn = pn.to_bytes(128, 'big')
	print(dpn)

	for i in dpn:
		print(chr(i), end='')
	print()


