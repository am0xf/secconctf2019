from Crypto.Util.number import *
from gmpy2 import *
from flag import flag

p = getPrime(512)
q = next_prime(p^1<<64)
e = 65537
phin = (p-1)*(q-1)
n = p*q

m = bytes_to_long(flag)
c = pow(m,e,n)

open('ciphertext','w').write(str((c,n)))
