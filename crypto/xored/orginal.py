import random
from flag import flag
import string

key = ''.join(random.choice(string.letters) for i in range(5))
#print "Key : " + key

bs=len(key)*2+1
assert len(flag)%bs < 5

while(len(flag)%bs!=0):
    flag+=" "

m=""
lk = list(key)
random.shuffle(lk)
key = ''.join(lk)

for i in range(len(flag)/bs) :
    new=""
    for j in range(bs) :
        if(j==0) :
            new+=chr(ord(flag[i*bs]) ^ord(key[1]))
        elif(j==(bs/2)) :
            new+=chr(ord(flag[i*bs+j])^ord(key[0]))
        elif(j%2==0) :
            new+=chr(ord(flag[i*bs+j])^ord(key[2]))
        elif((j==3) or (j==(bs-4))) :
            new+=chr(ord(flag[i*bs+ j])^ord(key[3]))
        elif((j==1) or (j==(bs-2))):
            new+=chr(ord(flag[i*bs+j])^ord(key[4]))
        else :
            print "Oh No!"
    m+=new

final=""
for j in range(len(m)) :
    final+="%02x"%ord(m[j])


print final 
