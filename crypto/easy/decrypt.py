#!/usr/bin/env python3

#from flag import flag,key

# brute key

def findn(k, d):
	for i in range(26):
		if (i * key) % 26 == d:
			return i

	return 0

def encrypt(data,key):
    cipher = b''
    for i in data:
        if i  in range(65,65+26):
            cipher+= bytes([findn(key, i-65)+65])
        elif i  in range(97,97+26):
            cipher += bytes([findn(key, i-97)+97])
        else:
            cipher += bytes([i])
    return cipher

#assert key <10
#assert encrypt(flag,key) == b'MukKsnKRZ{rju_puup_rjar_add_iun_xaq}'

for key in range(1, 10):
	s = b'MukKsnKRZ{rju_puup_rjar_add_iun_xaq}'
	d = encrypt(s, key)

	print(d)

