from flag import flag,key

def encrypt(data,key):
    cipher = b''
    for i in data:
        if i  in range(65,65+26):
            cipher+= bytes([(((i-65)*key)%26)+65])
        elif i  in range(97,97+26):
            cipher += bytes([(((i-97)*key)%26)+97])
        else:
            cipher += bytes([i])
    return cipher

assert key <10
assert encrypt(flag,key) == b'MukKsnKRZ{rju_puup_rjar_add_iun_xaq}'
